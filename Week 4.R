###########################################################################
#####                     Editing Text Variables                      #####
###########################################################################
fileUrl <- "https://data.baltimorecity.gov/api/views/dz54-2aru/rows.csv?accessType=DOWNLOAD"
download.file(fileUrl, destfile = '~/Workspace/Getting and Cleaning Data/cameras.csv',
              method = 'curl')
cameraData <- read.csv(file = '~/Workspace/Getting and Cleaning Data/cameras.csv')
names(cameraData)

## Fixing character vectors
# Turns all strings to lowercase
tolower( names( cameraData) )

# Split variable names
splitNames <- strsplit( names(cameraData), "\\." )


## Replaces characters in a string
fileUrl1 <- "https://dl.dropboxusercontent.com/u/7710864/data/reviews-apr29.csv"
fileUrl2 <- "https://dl.dropboxusercontent.com/u/7710864/data/solutions-apr29.csv"
download.file( fileUrl1, 
               destfile = '~/Workspace/Getting and Cleaning Data/reviews.csv', 
               method = 'curl')
download.file( fileUrl2, 
               destfile = '~/Workspace/Getting and Cleaning Data/solutions.csv', 
               method = 'curl')
reviews <- read.csv('~/Workspace/Getting and Cleaning Data/reviews.csv')
solutions <- read.csv( '~/Workspace/Getting and Cleaning Data/solutions.csv')
head(reviews, 2)

## Sub() Note: Just removes the first character it finds
sub( "_", "", names(reviews) )
## gsub() Note: removes all teh characters it finds
testName <- "this_is_a_test"
gsub("_", "", testName )

## Find values in a string and returns the index
grep("Alameda", cameraData$intersection)
## Find values in a string and returns true when finds it
table( grepl("Alameda", cameraData$intersection) )
## Find values in a string and returns the string
grep("Alameda", cameraData$intersection, value = T)

## More useful string functions
library(stringr)
# nchar 
nchar("Jeffery Leek")
# substr
substr( "Jeffery Leek", 1, 7 )
# paste
paste( "Jeffrey", "Leek")
# paste0 No space in between
paste0( "Jeffrey", "Leek")
# strtrim removes blank spaces in string
str_trim("Jeffrey     " )

###########################################################################
#####                      Regular Expressions                        #####
###########################################################################

# ^ is used to find characters in the beggining of a string. Ej. ^I think
# $ is used to find characters in the last of a string. Ej. morning$
# [] are used to find characters in all the line. Ej. [Bb]  
# Ranges can be specified. Ej. [a-z] or [a-zA-Z]
# Ej. [^?.]$ We are looking for anything that doesnt end in ? or .
# "." looks for any character. Ej. 9.11
# | is an or. Ej. flood|fire
# ()? Indicates optional. Ej. [Gg]eorge ([Ww]\.)? [Bb]ush will look for George Bush, and optionally George W. Bush.
# * or + are used to indicate repetion. * Is for any number, and + is at least one.
# {} are referred to as interval quantifiers. Specify minimum and maximum of matches. Ej. {1,4}
