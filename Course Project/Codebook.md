Codebook
========

The experiments have been carried out with a group of 30 volunteers within an age bracket of 19-48 years. Each person performed six activities (WALKING, WALKING_UPSTAIRS, WALKING_DOWNSTAIRS, SITTING, STANDING, LAYING) wearing a smartphone (Samsung Galaxy S II) on the waist. Using its embedded accelerometer and gyroscope, we captured 3-axial linear acceleration and 3-axial angular velocity at a constant rate of 50Hz. The experiments have been video-recorded to label the data manually. The obtained dataset has been randomly partitioned into two sets, where 70% of the volunteers was selected for generating the training data and 30% the test data. 

The sensor signals (accelerometer and gyroscope) were pre-processed by applying noise filters and then sampled in fixed-width sliding windows of 2.56 sec and 50% overlap (128 readings/window). The sensor acceleration signal, which has gravitational and body motion components, was separated using a Butterworth low-pass filter into body acceleration and gravity. The gravitational force is assumed to have only low frequency components, therefore a filter with 0.3 Hz cutoff frequency was used. From each window, a vector of features was obtained by calculating variables from the time and frequency domain. See 'features_info.txt' for more details. 


The repository includes the following files:
=========================================

- `README.md`: Contains the description of the procedure to obtain a tidy dataset. 

- `complete.txt`: The database with all the cases.

- `aggregate.txt`: The aggregated database that contains the averages by activity and subject.

- `tun_analysis.R`: The code used to get and clean the database.

Variables:
==========

- "tBodyAccMagMean": Mean of the body linear acceleration. Units (m/s^2)          
- "tBodyAccMagStd": Standard Deviation of the body linear acceleration. Units (m/s^2)           
- "tGravityAccMagMean": Mean of gravity acceleration signals using another low pass Butterworth filter with a corner frequency of 0.3 Hz.     
- "tGravityAccMagStd": Standard Deviation of gravity acceleration signals using another low pass Butterworth filter with a corner frequency of 0.3 Hz.     
- "tBodyAccJerkMagMean":  Mean of Fast Fourier Transform (FFT) was applied to some of these signals. Units (Hz).
- "tBodyAccJerkMagStd": Standard Deviation of Fast Fourier Transform (FFT) was applied to some of these signals. Units (Hz).     
- "tBodyGyroMagMean": Mean of Fast Fourier Transform (FFT) was applied to some of these signals. Units (Hz).        
- "tBodyGyroMagStd": Mean of Fast Fourier Transform (FFT) was applied to some of these signals. Units (Hz).         
- "tBodyGyroJerkMagMean": Mean of Fast Fourier Transform (FFT) was applied to some of these signals. Units (Hz).    
- "tBodyGyroJerkMagStd": Mean of Fast Fourier Transform (FFT) was applied to some of these signals. Units (Hz).     
- "fBodyAccMagMean": Mean of Fast Fourier Transform (FFT) was applied to some of these signals. Units (Hz).         
- "fBodyAccMagStd": Mean of Fast Fourier Transform (FFT) was applied to some of these signals. Units (Hz).         
- "fBodyBodyAccJerkMagMean": Mean of Fast Fourier Transform (FFT) was applied to some of these signals. Units (Hz). 
- "fBodyBodyAccJerkMagStd": Mean of Fast Fourier Transform (FFT) was applied to some of these signals. Units (Hz).  
- "fBodyBodyGyroMagMean": Mean of Fast Fourier Transform (FFT) was applied to some of these signals. Units (Hz).   
- "fBodyBodyGyroMagStd": Mean of Fast Fourier Transform (FFT) was applied to some of these signals. Units (Hz).     
- "fBodyBodyGyroJerkMagMean": Mean of Fast Fourier Transform (FFT) was applied to some of these signals. Units (Hz).
- "fBodyBodyGyroJerkMagStd": Mean of Fast Fourier Transform (FFT) was applied to some of these signals. Units (Hz). 
- "activity": Is the type of activity that the subjects are performing. Is a factor.     
