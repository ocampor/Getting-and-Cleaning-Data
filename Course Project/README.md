# Course Project: Getting and Cleaning Data
Ricardo Ocampo  
06/17/2015  



### Summary

In this project several files were merged in a complete database. The tidy dataset rules were followed. 
Also a dataset with the mean of the subjects and activities is provided. The data is described in the
`CoodBook.md` file.

# Procedure

## Read of all the files.

First, all the files were red. In this case, there were 8 files (`X_train.txt`, `y_train.txt`, `X_test.txt`
`y_test.txt`, `subject_train.txt`, `subject_test.txt`, `features.txt`, `activity_labels.txt`). All the 
files were needed to solve the questions.


```r
Xtrain <- read.table( file = file.path( folderPath, "train", "X_train.txt" ) )
ytrain <- read.table( file = file.path( folderPath, "train", "y_train.txt") )
Xtest <- read.table( file = file.path( folderPath, "test", "X_test.txt" ) )
ytest <- read.table( file = file.path( folderPath, "test", "y_test.txt" ) )
subjectTrain <- read.table( file = file.path( folderPath, "train", "subject_train.txt" ) )
subjectTest <- read.table( file = file.path( folderPath, "test", "subject_test.txt" ) )
varNames <- read.table( file = file.path( folderPath, "features.txt") )
activityNames <- read.table( file = file.path( folderPath, "activity_labels.txt" ) )
```

## Merge of training and test setes to create one data set.

After all the files are loaded. We have to bind the different data bases. For the moment, we just
merge the X and y of test and train databases.

```r
dataSet <- cbind( rbind( Xtrain, Xtest ), rbind( ytrain, ytest ) )
str( dataSet )
```

```
## 'data.frame':	10299 obs. of  562 variables:
##  $ V1  : num  0.289 0.278 0.28 0.279 0.277 ...
##  $ V2  : num  -0.0203 -0.0164 -0.0195 -0.0262 -0.0166 ...
##  $ V3  : num  -0.133 -0.124 -0.113 -0.123 -0.115 ...
##  $ V4  : num  -0.995 -0.998 -0.995 -0.996 -0.998 ...
##  $ V5  : num  -0.983 -0.975 -0.967 -0.983 -0.981 ...
##  $ V6  : num  -0.914 -0.96 -0.979 -0.991 -0.99 ...
##  $ V7  : num  -0.995 -0.999 -0.997 -0.997 -0.998 ...
##  $ V8  : num  -0.983 -0.975 -0.964 -0.983 -0.98 ...
##  $ V9  : num  -0.924 -0.958 -0.977 -0.989 -0.99 ...
##  $ V10 : num  -0.935 -0.943 -0.939 -0.939 -0.942 ...
##  $ V11 : num  -0.567 -0.558 -0.558 -0.576 -0.569 ...
##  $ V12 : num  -0.744 -0.818 -0.818 -0.83 -0.825 ...
##  $ V13 : num  0.853 0.849 0.844 0.844 0.849 ...
##  $ V14 : num  0.686 0.686 0.682 0.682 0.683 ...
##  $ V15 : num  0.814 0.823 0.839 0.838 0.838 ...
##  $ V16 : num  -0.966 -0.982 -0.983 -0.986 -0.993 ...
##  $ V17 : num  -1 -1 -1 -1 -1 ...
##  $ V18 : num  -1 -1 -1 -1 -1 ...
##  $ V19 : num  -0.995 -0.998 -0.999 -1 -1 ...
##  $ V20 : num  -0.994 -0.999 -0.997 -0.997 -0.998 ...
##  $ V21 : num  -0.988 -0.978 -0.965 -0.984 -0.981 ...
##  $ V22 : num  -0.943 -0.948 -0.975 -0.986 -0.991 ...
##  $ V23 : num  -0.408 -0.715 -0.592 -0.627 -0.787 ...
##  $ V24 : num  -0.679 -0.501 -0.486 -0.851 -0.559 ...
##  $ V25 : num  -0.602 -0.571 -0.571 -0.912 -0.761 ...
##  $ V26 : num  0.9293 0.6116 0.273 0.0614 0.3133 ...
##  $ V27 : num  -0.853 -0.3295 -0.0863 0.0748 -0.1312 ...
##  $ V28 : num  0.36 0.284 0.337 0.198 0.191 ...
##  $ V29 : num  -0.0585 0.2846 -0.1647 -0.2643 0.0869 ...
##  $ V30 : num  0.2569 0.1157 0.0172 0.0725 0.2576 ...
##  $ V31 : num  -0.2248 -0.091 -0.0745 -0.1553 -0.2725 ...
##  $ V32 : num  0.264 0.294 0.342 0.323 0.435 ...
##  $ V33 : num  -0.0952 -0.2812 -0.3326 -0.1708 -0.3154 ...
##  $ V34 : num  0.279 0.086 0.239 0.295 0.44 ...
##  $ V35 : num  -0.4651 -0.0222 -0.1362 -0.3061 -0.2691 ...
##  $ V36 : num  0.4919 -0.0167 0.1739 0.4821 0.1794 ...
##  $ V37 : num  -0.191 -0.221 -0.299 -0.47 -0.089 ...
##  $ V38 : num  0.3763 -0.0134 -0.1247 -0.3057 -0.1558 ...
##  $ V39 : num  0.4351 -0.0727 -0.1811 -0.3627 -0.1898 ...
##  $ V40 : num  0.661 0.579 0.609 0.507 0.599 ...
##  $ V41 : num  0.963 0.967 0.967 0.968 0.968 ...
##  $ V42 : num  -0.141 -0.142 -0.142 -0.144 -0.149 ...
##  $ V43 : num  0.1154 0.1094 0.1019 0.0999 0.0945 ...
##  $ V44 : num  -0.985 -0.997 -1 -0.997 -0.998 ...
##  $ V45 : num  -0.982 -0.989 -0.993 -0.981 -0.988 ...
##  $ V46 : num  -0.878 -0.932 -0.993 -0.978 -0.979 ...
##  $ V47 : num  -0.985 -0.998 -1 -0.996 -0.998 ...
##  $ V48 : num  -0.984 -0.99 -0.993 -0.981 -0.989 ...
##  $ V49 : num  -0.895 -0.933 -0.993 -0.978 -0.979 ...
##  $ V50 : num  0.892 0.892 0.892 0.894 0.894 ...
##  $ V51 : num  -0.161 -0.161 -0.164 -0.164 -0.167 ...
##  $ V52 : num  0.1247 0.1226 0.0946 0.0934 0.0917 ...
##  $ V53 : num  0.977 0.985 0.987 0.987 0.987 ...
##  $ V54 : num  -0.123 -0.115 -0.115 -0.121 -0.122 ...
##  $ V55 : num  0.0565 0.1028 0.1028 0.0958 0.0941 ...
##  $ V56 : num  -0.375 -0.383 -0.402 -0.4 -0.4 ...
##  $ V57 : num  0.899 0.908 0.909 0.911 0.912 ...
##  $ V58 : num  -0.971 -0.971 -0.97 -0.969 -0.967 ...
##  $ V59 : num  -0.976 -0.979 -0.982 -0.982 -0.984 ...
##  $ V60 : num  -0.984 -0.999 -1 -0.996 -0.998 ...
##  $ V61 : num  -0.989 -0.99 -0.992 -0.981 -0.991 ...
##  $ V62 : num  -0.918 -0.942 -0.993 -0.98 -0.98 ...
##  $ V63 : num  -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 ...
##  $ V64 : num  -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 ...
##  $ V65 : num  0.114 -0.21 -0.927 -0.596 -0.617 ...
##  $ V66 : num  -0.59042 -0.41006 0.00223 -0.06493 -0.25727 ...
##  $ V67 : num  0.5911 0.4139 0.0275 0.0754 0.2689 ...
##  $ V68 : num  -0.5918 -0.4176 -0.0567 -0.0858 -0.2807 ...
##  $ V69 : num  0.5925 0.4213 0.0855 0.0962 0.2926 ...
##  $ V70 : num  -0.745 -0.196 -0.329 -0.295 -0.167 ...
##  $ V71 : num  0.7209 0.1253 0.2705 0.2283 0.0899 ...
##  $ V72 : num  -0.7124 -0.1056 -0.2545 -0.2063 -0.0663 ...
##  $ V73 : num  0.7113 0.1091 0.2576 0.2048 0.0671 ...
##  $ V74 : num  -0.995 -0.834 -0.705 -0.385 -0.237 ...
##  $ V75 : num  0.996 0.834 0.714 0.386 0.239 ...
##  $ V76 : num  -0.996 -0.834 -0.723 -0.387 -0.241 ...
##  $ V77 : num  0.992 0.83 0.729 0.385 0.241 ...
##  $ V78 : num  0.57 -0.831 -0.181 -0.991 -0.408 ...
##  $ V79 : num  0.439 -0.866 0.338 -0.969 -0.185 ...
##  $ V80 : num  0.987 0.974 0.643 0.984 0.965 ...
##  $ V81 : num  0.078 0.074 0.0736 0.0773 0.0734 ...
##  $ V82 : num  0.005 0.00577 0.0031 0.02006 0.01912 ...
##  $ V83 : num  -0.06783 0.02938 -0.00905 -0.00986 0.01678 ...
##  $ V84 : num  -0.994 -0.996 -0.991 -0.993 -0.996 ...
##  $ V85 : num  -0.988 -0.981 -0.981 -0.988 -0.988 ...
##  $ V86 : num  -0.994 -0.992 -0.99 -0.993 -0.992 ...
##  $ V87 : num  -0.994 -0.996 -0.991 -0.994 -0.997 ...
##  $ V88 : num  -0.986 -0.979 -0.979 -0.986 -0.987 ...
##  $ V89 : num  -0.993 -0.991 -0.987 -0.991 -0.991 ...
##  $ V90 : num  -0.985 -0.995 -0.987 -0.987 -0.997 ...
##  $ V91 : num  -0.992 -0.979 -0.979 -0.992 -0.992 ...
##  $ V92 : num  -0.993 -0.992 -0.992 -0.99 -0.99 ...
##  $ V93 : num  0.99 0.993 0.988 0.988 0.994 ...
##  $ V94 : num  0.992 0.992 0.992 0.993 0.993 ...
##  $ V95 : num  0.991 0.989 0.989 0.993 0.986 ...
##  $ V96 : num  -0.994 -0.991 -0.988 -0.993 -0.994 ...
##  $ V97 : num  -1 -1 -1 -1 -1 ...
##  $ V98 : num  -1 -1 -1 -1 -1 ...
##  $ V99 : num  -1 -1 -1 -1 -1 ...
##   [list output truncated]
```

## Extracts only the measurements on the mean and standard deviation for each measurement. 

The resulting database contains 561 variables plus the class. Therefore, a selection of
variables is requiered. For this analysis, we selected all the averages and standard deviation variables.

```r
extractedVars <- grep(pattern = "mean\\()$|std\\()$", varNames$V2 )
extractedVars <- c( extractedVars, ncol( dataSet ) )
dataSet <- dataSet[ extractedVars ]
varNames <- varNames$V2[ extractedVars ]
varNames
```

```
##  [1] tBodyAccMag-mean()          tBodyAccMag-std()          
##  [3] tGravityAccMag-mean()       tGravityAccMag-std()       
##  [5] tBodyAccJerkMag-mean()      tBodyAccJerkMag-std()      
##  [7] tBodyGyroMag-mean()         tBodyGyroMag-std()         
##  [9] tBodyGyroJerkMag-mean()     tBodyGyroJerkMag-std()     
## [11] fBodyAccMag-mean()          fBodyAccMag-std()          
## [13] fBodyBodyAccJerkMag-mean()  fBodyBodyAccJerkMag-std()  
## [15] fBodyBodyGyroMag-mean()     fBodyBodyGyroMag-std()     
## [17] fBodyBodyGyroJerkMag-mean() fBodyBodyGyroJerkMag-std() 
## [19] <NA>                       
## 477 Levels: angle(tBodyAccJerkMean),gravityMean) ...
```

## Uses descriptive activity names to name the activities in the data set

In order to have a factor that is clearer. We add the labels for each of the levels.

```r
dataSet["V1"] <- factor( dataSet$V1, labels = activityNames$V2 )
str( dataSet )
```

```
## 'data.frame':	10299 obs. of  19 variables:
##  $ V201: num  -0.959 -0.979 -0.984 -0.987 -0.993 ...
##  $ V202: num  -0.951 -0.976 -0.988 -0.986 -0.991 ...
##  $ V214: num  -0.959 -0.979 -0.984 -0.987 -0.993 ...
##  $ V215: num  -0.951 -0.976 -0.988 -0.986 -0.991 ...
##  $ V227: num  -0.993 -0.991 -0.989 -0.993 -0.993 ...
##  $ V228: num  -0.994 -0.992 -0.99 -0.993 -0.996 ...
##  $ V240: num  -0.969 -0.981 -0.976 -0.982 -0.985 ...
##  $ V241: num  -0.964 -0.984 -0.986 -0.987 -0.989 ...
##  $ V253: num  -0.994 -0.995 -0.993 -0.996 -0.996 ...
##  $ V254: num  -0.991 -0.996 -0.995 -0.995 -0.995 ...
##  $ V503: num  -0.952 -0.981 -0.988 -0.988 -0.994 ...
##  $ V504: num  -0.956 -0.976 -0.989 -0.987 -0.99 ...
##  $ V516: num  -0.994 -0.99 -0.989 -0.993 -0.996 ...
##  $ V517: num  -0.994 -0.992 -0.991 -0.992 -0.994 ...
##  $ V529: num  -0.98 -0.988 -0.989 -0.989 -0.991 ...
##  $ V530: num  -0.961 -0.983 -0.986 -0.988 -0.989 ...
##  $ V542: num  -0.992 -0.996 -0.995 -0.995 -0.995 ...
##  $ V543: num  -0.991 -0.996 -0.995 -0.995 -0.995 ...
##  $ V1  : Factor w/ 6 levels "WALKING","WALKING_UPSTAIRS",..: 5 5 5 5 5 5 5 5 5 5 ...
```

## Appropriately labels the data set with descriptive variable names. 

Each variable contains names that are not tidy. Hence, the variable names were modified to be clearer
for the user and also asigned to the database.

```r
varNames <- gsub( pattern = "(-|\\(\\))", replacement = "", x = varNames )
varNames <- sub( pattern = "mean", "Mean", x = varNames )
varNames <- sub( pattern = "std", "Std", x = varNames )
varNames[ length( varNames ) ] <- "activity"
colnames( dataSet ) <- varNames 
colnames( dataSet )
```

```
##  [1] "tBodyAccMagMean"          "tBodyAccMagStd"          
##  [3] "tGravityAccMagMean"       "tGravityAccMagStd"       
##  [5] "tBodyAccJerkMagMean"      "tBodyAccJerkMagStd"      
##  [7] "tBodyGyroMagMean"         "tBodyGyroMagStd"         
##  [9] "tBodyGyroJerkMagMean"     "tBodyGyroJerkMagStd"     
## [11] "fBodyAccMagMean"          "fBodyAccMagStd"          
## [13] "fBodyBodyAccJerkMagMean"  "fBodyBodyAccJerkMagStd"  
## [15] "fBodyBodyGyroMagMean"     "fBodyBodyGyroMagStd"     
## [17] "fBodyBodyGyroJerkMagMean" "fBodyBodyGyroJerkMagStd" 
## [19] "activity"
```


## From the data set in step 4, creates a second, independent tidy data set with the average of each variable for each activity and each subject.

Finally, an aggregated table with the averages of all the variables grouped by activity and subjects is 
calculated.

```r
subjects <- rbind( subjectTrain, subjectTest )
dataSet$subjects <- subjects$V1
aggregate_table <- aggregate(by = list( dataSet$activity, dataSet$subjects ), 
                             FUN = mean, x = subset( dataSet, select = -c(subjects, activity ) )  )
aggregate_table <- rename(.data = aggregate_table,  activity = Group.1,  subjects = Group.2 )
head(aggregate_table)
```

```
##             activity subjects tBodyAccMagMean tBodyAccMagStd
## 1            WALKING        1     -0.13697118    -0.21968865
## 2   WALKING_UPSTAIRS        1     -0.12992763    -0.32497093
## 3 WALKING_DOWNSTAIRS        1      0.02718829     0.01988435
## 4            SITTING        1     -0.94853679    -0.92707842
## 5           STANDING        1     -0.98427821    -0.98194293
## 6             LAYING        1     -0.84192915    -0.79514486
##   tGravityAccMagMean tGravityAccMagStd tBodyAccJerkMagMean
## 1        -0.13697118       -0.21968865         -0.14142881
## 2        -0.12992763       -0.32497093         -0.46650345
## 3         0.02718829        0.01988435         -0.08944748
## 4        -0.94853679       -0.92707842         -0.98736420
## 5        -0.98427821       -0.98194293         -0.99236779
## 6        -0.84192915       -0.79514486         -0.95439626
##   tBodyAccJerkMagStd tBodyGyroMagMean tBodyGyroMagStd tBodyGyroJerkMagMean
## 1        -0.07447175      -0.16097955      -0.1869784           -0.2987037
## 2        -0.47899162      -0.12673559      -0.1486193           -0.5948829
## 3        -0.02578772      -0.07574125      -0.2257244           -0.2954638
## 4        -0.98412002      -0.93089249      -0.9345318           -0.9919763
## 5        -0.99309621      -0.97649379      -0.9786900           -0.9949668
## 6        -0.92824563      -0.87475955      -0.8190102           -0.9634610
##   tBodyGyroJerkMagStd fBodyAccMagMean fBodyAccMagStd
## 1          -0.3253249     -0.12862345     -0.3980326
## 2          -0.6485530     -0.35239594     -0.4162601
## 3          -0.3065106      0.09658453     -0.1865303
## 4          -0.9883087     -0.94778292     -0.9284448
## 5          -0.9947332     -0.98535636     -0.9823138
## 6          -0.9358410     -0.86176765     -0.7983009
##   fBodyBodyAccJerkMagMean fBodyBodyAccJerkMagStd fBodyBodyGyroMagMean
## 1             -0.05711940             -0.1034924           -0.1992526
## 2             -0.44265216             -0.5330599           -0.3259615
## 3              0.02621849             -0.1040523           -0.1857203
## 4             -0.98526213             -0.9816062           -0.9584356
## 5             -0.99254248             -0.9925360           -0.9846176
## 6             -0.93330036             -0.9218040           -0.8621902
##   fBodyBodyGyroMagStd fBodyBodyGyroJerkMagMean fBodyBodyGyroJerkMagStd
## 1          -0.3210180               -0.3193086              -0.3816019
## 2          -0.1829855               -0.6346651              -0.6939305
## 3          -0.3983504               -0.2819634              -0.3919199
## 4          -0.9321984               -0.9897975              -0.9870496
## 5          -0.9784661               -0.9948154              -0.9946711
## 6          -0.8243194               -0.9423669              -0.9326607
```

